function capitalize(input) {
	if (typeof input !== "string") {
		throw new Error("bad input");
	}
	return input.charAt(0).toUpperCase() + input.slice(1);
}
module.exports = capitalize;
const user = require("../src/user");

it("snapshot testing, passing 1 as ID", () => {
	expect(user(1)).toMatchSnapshot();
});
it("snapshot testing, passing 56 as ID", () => {
	expect(user(56)).toMatchSnapshot();
});
it("snapshot testing, passing 1345 as ID", () => {
	expect(user(1345)).toMatchSnapshot();
});
it("snapshot testing, passing a string as ID", () => {
	expect(() => {
		user("a");
	}).toThrow();
});